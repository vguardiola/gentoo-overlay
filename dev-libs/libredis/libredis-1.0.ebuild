# Copyright 1999-2014 Victor Guardiola <victor.guardiola@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit git-2
KEYWORDS="~amd64 ~mips ~ppc ~ppc64 ~x86"
HOMEPAGE="https://github.com/hyves-org/libredis"
DESCRIPTION="A C based general low-level PHP extension and client library for Redis, focusing on performance, generality and efficient parallel communication with multiple Redis servers. As a bonus, a Ketama Consistent Hashing implementation is provided as well."
LICENSE="BSD"
EGIT_REPO_URI="git://github.com/hyves-org/libredis.git"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~mips ~ppc ~ppc64 ~x86"
IUSE=""
LICENSE="BSD"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"
