# Copyright 2012 Victor Guardiola <victor.guardiola@gmail.com>
# Distributed under the terms of the GNU General Public License v2

EAPI=4
PHP_EXT_NAME="phalcon"
PHP_EXT_PECL_PKG="phalcon"
PHP_EXT_INI="yes"
PHP_EXT_ZENDEXT="no"
DOCS=""
PHP_EXT_INIFILE="phalcon.ini"
USE_PHP="php5-3"
inherit php-ext-pecl-r2 git-2
SRC_URI=""
EGIT_REPO_URI="git://github.com/phalcon/cphalcon.git"
EGIT_BRANCH="0.4.5"
KEYWORDS="~amd64 ~mips ~ppc ~ppc64 ~x86"
HOMEPAGE="http://www.phalconphp.com"
DESCRIPTION="Phalcon PHP is a web framework delivered as a C extension providing high performance and lower resource consumption"
LICENSE="BSD"
SLOT="0"
DEPEND=">=dev-php/PEAR-PEAR-1.9.1
		dev-lang/php[mysqli]"
RDEPEND="${DEPEND}"
FIX_S="${WORKDIR}/${PHP_EXT_NAME}-${PV}/release/"
src_prepare () {
	cd ${FIX_S}
    php-ext-source-r2_src_prepare
}
src_configure() {
	cd ${FIX_S}
	my_conf="--enable-phalcon"
	php-ext-source-r2_src_configure
}
src_compile () {
	cd ${FIX_S}
    php-ext-source-r2_src_compile
}
src_install () {
	cd ${FIX_S}
    php-ext-source-r2_src_install
}