# Copyright 2012 Victor Guardiola <victor.guardiola@gmail.com>
# Distributed under the terms of the GNU General Public License v2

EAPI=4
DOCS=""
SRC_URI=""
inherit git-2
EGIT_REPO_URI="git://github.com/phalcon/zephir.git"
EGIT_BRANCH=""
KEYWORDS="~amd64 ~x86"
HOMEPAGE="http://zephir-lang.com/"
DESCRIPTION=" Zephir, an open source, high-level/domain specific language designed to ease the creation and maintainability of extensions for PHP with a focus on type and memory safety."
LICENSE="BSD"
SLOT="0"
DEPEND="dev-libs/json-c
dev-libs/libpcre
>=sys-devel/gcc-4.0
>=dev-util/re2c-0.13
>=sys-devel/clang-3
>=sys-devel/make-3.81
>=sys-devel/autoconf-2.31
>=sys-devel/automake-1.14"
RDEPEND="${DEPEND}"
src_configure() {
    sed -i s/" -g3 "/" -I\/usr\/include\/json-c -g3 "/g install
    sed -i s/"-ljson "/""/g install
    sed -i s/"bash"/"bash\nZEPHIRDIR=\"\/usr\/share\/zephir\/\""/g bin/zephir
    sed -i s/"export CC=\"gcc\""/"aclocal \&\& libtoolize \-\-force \&\& autoreconf \&\& export CC=\"gcc\""/g Library/Compiler.php
}
src_install () {
    insinto /usr/share/zephir
    ./install -c
    doins -r *
    fperms 755 /usr/share/zephir/bin/*
}