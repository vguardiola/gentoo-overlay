# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header $

EAPI="5"
PHP_EXT_NAME="redis"
USE_PHP="php5-4 php5-5 php5-6"

inherit php-ext-source-r2

MY_P="phpredis-2.2.7"

DESCRIPTION="A PHP extension for Redis"
HOMEPAGE="http://github.com/phpredis/phpredis"
SRC_URI="https://github.com/phpredis/phpredis/archive/2.2.7.zip -> ${P}.zip"
DOCS="README.markdown CREDITS COPYING arrays.markdown"

LICENSE="PHP-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="tests igbinary"

#DEPEND=""
#RDEPEND="dev-lang/php"

S="${WORKDIR}/${MY_P}"
PHP_EXT_S=${S}