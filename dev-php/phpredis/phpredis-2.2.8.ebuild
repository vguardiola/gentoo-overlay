# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header $

EAPI="5"
PHP_EXT_NAME="redis"
USE_PHP="php7-0"

inherit php-ext-source-r2 git-2

MY_P="phpredis-2.2.8"

DESCRIPTION="A PHP extension for Redis"
HOMEPAGE="http://github.com/phpredis/phpredis"
SRC_URI=""
EGIT_REPO_URI="git://github.com/phpredis/phpredis.git"
EGIT_BRANCH="php7"

DOCS="README.markdown CREDITS COPYING arrays.markdown cluster.markdown"

LICENSE="PHP-3"
SLOT="7"
KEYWORDS="~amd64 ~x86"
IUSE="tests igbinary"

#DEPEND=""
#RDEPEND="dev-lang/php"

S="${WORKDIR}/${MY_P}"
PHP_EXT_S=${S}
