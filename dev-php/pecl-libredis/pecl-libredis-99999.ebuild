# Copyright 2012 Victor Guardiola <victor.guardiola@gmail.com>
# Distributed under the terms of the GNU General Public License v2

EAPI=4
PHP_EXT_NAME="libredis"
PHP_EXT_PECL_PKG="libredis"
PHP_EXT_ZENDEXT="no"
DOCS=""
USE_PHP="php5-4 php5-5"
inherit php-ext-pecl-r2 git-2
SRC_URI=""
EGIT_REPO_URI="git://github.com/hyves-org/libredis.git"
KEYWORDS="~amd64 ~mips ~ppc ~ppc64 ~x86"
HOMEPAGE="https://github.com/hyves-org/libredis"
DESCRIPTION="A C based general low-level PHP extension and client library for Redis, focusing on performance, generality and efficient parallel communication with multiple Redis servers. As a bonus, a Ketama Consistent Hashing implementation is provided as well."
LICENSE="BSD"
SLOT="0"
DEPEND="dev-libs/libredis"
RDEPEND="${DEPEND}"
src_prepare () {
    cd ${WORKDIR}/
	mkdir php5.{4,5}
	cp -r ${PHP_EXT_NAME}-${PV}/php/ext/libredis/* php5.4/
	cp -r ${PHP_EXT_NAME}-${PV}/php/ext/libredis/* php5.5/
    php-ext-source-r2_src_prepare
}
src_configure() {
	php-ext-source-r2_src_configure
}
src_compile () {
    php-ext-source-r2_src_compile
}
src_install () {
    php-ext-source-r2_src_install
}