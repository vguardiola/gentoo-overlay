My overlay

1. Edit overlays section of `/etc/layman/layman.cfg`. Here's an example:

        overlays: http://www.gentoo.org/proj/en/overlays/repositories.xml
                  https://bitbucket.org/vguardiola/gentoo-overlay/raw/9678cd272589eb6f37ab07c807f1e4ee0075c810/overlay.xml

2. Execute following command:

        layman -a vgl-devops

Actual packages

	├── app-emulation
	│   └── wine
	│   └── genymotion
	├── app-misc
	│   └── Leap
	├── dev-libs
	│   └── libredis
	├── dev-php
	│   ├── hhvm
	│   ├── pecl-apcu
	│   ├── pecl-libredis
	│   ├── pecl-phalconphp
	│   ├── phpredis
	│   └── zephir
	├── dev-util
	│   ├── leapmotion-drivers
	│   └── leapmotion-sdk
	├── kde-misc
	│   └── kde-thumbnailer-epub
	├── mail-mta
	│   └── mailcatcher	
	├── net-im
	│   └── ejabberd	
	└── sys-cluster
	    ├── libres3
	    └── sx

